package com.tavisca.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class KafkademoApplication {

	private static Logger log = LoggerFactory.getLogger(KafkademoApplication.class);

	//@Autowired
	private MessagingAppp messagingAppp;

	public static void main(String[] args) throws  Exception{

		ConfigurableApplicationContext context =  SpringApplication.run(KafkademoApplication.class, args);
		MessagingAppp messagingAppp =  context.getBean(MessagingAppp.class);
		for (int i=0; i<5 ; i++){

			messagingAppp.sendMessage("Source Message  "+ i, i );
			Thread.sleep(3000);
		}
		//log.info("Messages published to the Topic - Done!");


	}


}
