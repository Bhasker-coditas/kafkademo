package com.tavisca.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Autowired MessagingAppp messagingAppp;

    @GetMapping("/publish")
    public String publishMessage(@RequestParam String msg, @RequestParam int partition) throws Exception {

        for (int i=0; i<5 ; i++){

            messagingAppp.sendMessage("Source Message  "+ i, -1 );
            Thread.sleep(3000);
        }
        return "Sent!";

    }
}
