package com.tavisca.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.security.Timestamp;
import java.util.Date;

@Component
public class MessagingAppp {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value(value = "${partitioned.topic.name}")
    private String topicName;

    private static Logger log = LoggerFactory.getLogger(MessagingAppp.class);



    public void sendMessage(String msg, int partition) {
        if(partition>-1){
            log.info("Writing message to partition - " + partition);
            kafkaTemplate.send(topicName, partition, "KEY-"+String.valueOf(partition), msg);
        } else {
            kafkaTemplate.send(topicName, msg);
        }
    }

    @KafkaListener(topicPartitions = @TopicPartition(topic = "paymentrequest", partitions = { "0" }
            /*partitionOffsets = {@PartitionOffset(partition = "0", initialOffset = "0"),
                                @PartitionOffset(partition = "3", initialOffset = "0")}*/
    )
    )
    public void listenToParition0( @Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                                   ConsumerRecord<?, ?> record) {
        log.info("Consumer 0 Received : " + message +" :  from partition : " + partition  + " with key : "+ key);
    }

    @KafkaListener(topicPartitions = @TopicPartition(topic = "paymentrequest", partitions = { "1" }
            /*partitionOffsets = {@PartitionOffset(partition = "0", initialOffset = "0"),
                                @PartitionOffset(partition = "3", initialOffset = "0")}*/
    )
    )
    public void listenToParition1( @Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                                   ConsumerRecord<?, ?> record) {
        log.info("Consumer 1 Received : " + message +" :  from partition : " + partition  + " with key : "+ key);
    }


    @KafkaListener(topicPartitions = @TopicPartition(topic = "paymentrequest", partitions = { "2" })
    )
    public void listenToParition2( @Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                                   ConsumerRecord<?, ?> record) {
        log.info("Consumer 2 Received : " + message +" :  from partition : " + partition + " with key : "+ key);
    }

    @KafkaListener(topicPartitions = @TopicPartition(topic = "paymentrequest", partitions = { "3" }))
    public void listenToParition3( @Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                                   ConsumerRecord<?, ?> record) {
        log.info("Consumer 3 Received : " + message +" :  from partition : " + partition + " with key : "+ key);
    }


    @KafkaListener(topicPartitions = @TopicPartition(topic = "paymentrequest", partitions = { "4" }))
    public void listenToParition4( @Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
                                   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                                   ConsumerRecord<?, ?> record) {
        log.info("Consumer 4 Received : " + message +" :  from partition : " + partition + " with key : "+ key);
    }
}
